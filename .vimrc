vim9script

plug#begin()

# Auto complete
Plug 'honza/vim-snippets'
Plug 'SirVer/ultisnips'
Plug 'neoclide/coc.nvim', {'branch': 'master', 'do': 'npm ci'}

Plug 'vim-autoformat/vim-autoformat'

# UI
Plug 'itchyny/lightline.vim'
Plug 'morhetz/gruvbox'
Plug 'dracula/vim'
Plug 'altercation/vim-colors-solarized'
Plug 'nanotech/jellybeans.vim'
Plug 'preservim/nerdtree'
Plug 'sheerun/vim-polyglot'
Plug 'pineapplegiant/spaceduck'
Plug 'mhinz/vim-startify'

# Parenteses, chaves, colchetes...
Plug 'townk/vim-autoclose'

# Coisas do Tim Pope :)
Plug 'tpope/vim-rails'
Plug 'tpope/vim-rake'
Plug 'tpope/vim-rbenv'
Plug 'tpope/vim-endwise'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'

plug#end()

### --- Configurações --- ###
set nobackup
set nowritebackup
set encoding=utf-8
set autoread
set confirm
set colorcolumn=80
set signcolumn=yes
set backspace=2
set clipboard=unnamedplus
set hidden
set nocompatible
set showcmd
set nowrap
set hlsearch
set undolevels=3000

# --- Cursor --- #
set cursorline
set scrolloff=999

# --- Aparencia --- #
g:solarized_termcolors = 16
syntax enable
set background=dark

if exists('+termguicolors')
	&t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
	&t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
	set termguicolors
endif

colorscheme dracula
set fillchars+=vert:\▏

# --- Números --- #
set number
set relativenumber

# --- Identação --- #
set tabstop=2
set softtabstop=2
set shiftwidth=2
set autoindent

# --- Busca --- #
set smartcase
set incsearch
set wildmenu

### --- AutoCMD --- ###
autocmd BufWritePost * :Autoformat

### Mapeamentos ###
g:mapleader = ','
nmap <leader>e :NERDTree<CR>

# Mover entre buffers
nmap <C-h> <C-w>h
nmap <C-j> <C-w>j
nmap <C-k> <C-w>k
nmap <C-l> <C-w>l

# Dividir buffers
nmap <leader>bs :split<CR>
nmap <leader>bv :vsplit<CR>
